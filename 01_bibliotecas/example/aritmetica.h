#ifndef __ARITMETICA_H__
#define __ARITMETICA_H__

#ifdef __cplusplus
extern "C" {
#endif

    int suma           (int op1, int op2);
    int resta          (int op1, int op2);
    int multiplicacion (int op1, int op2);
    int division       (int op1, int op2);

#ifdef __cplusplus
}
#endif

#endif
