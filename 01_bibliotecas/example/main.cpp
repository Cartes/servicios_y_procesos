#include <stdio.h>
#include <stdlib.h>

#include "aritmetica.h"

int main ()
{

    printf("La suma de 5 y 2 es: %i\n", suma(5,2));
    printf("La resta de 5 y 2 es: %i\n", resta(5,2));
    printf("La multiplicacion entre 5 y 2 es: %i\n", multiplicacion(5,2));
    printf("La division entre 6 y 2 es: %i\n", division(6,2));


    return 0;

}
