#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <string.h>

#include "aritmetica.h"

int main () {

    void *handle;

    handle = dlopen ("./libaritmetica.so", RTLD_LAZY);

    if (!handle) {
        fprintf(stderr, "%s\n", dlerror());
        return EXIT_FAILURE;
    }

    dlerror();

    void (*aritmetica)() = (void (*)()) dlsym (handle, "suma");
    (*aritmetica)();

    void (*resta)() = (void (*)()) dlsym (handle, "resta");
    (*resta)();


    dlclose(handle);

    printf("%i\n", *aritmetica);

    printf("%i\n", *resta);
    return 0;
};

