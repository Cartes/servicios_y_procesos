#include <pthread.h>
#include <stdio.h>

#include "hilo.h"
int main()
{
    pthread_t thread1_id;
    pthread_t thread2_id;
    struct TPila thread1_args;
    struct TPila thread2_args;

    thread1_args.character = 'x';
    thread1_args.count = 30000;
    pthread_create (&thread1_id, NULL, &char_print, &thread1_args); //en thread1_id se aloja el id de hilo creado

    thread2_args.character = 'o';
    thread2_args.count = 20000;        //char_printFuncion que se ejecuta dentro del hilo
    pthread_create (&thread2_id, NULL, &char_print, &thread2_args);
                                //NULL:configuración hilo

    //Con la funcion pthread_join nos aseguramos de cerrar el hilo // wait
    pthread_join (thread1_id, NULL);
    pthread_join (thread2_id, NULL);

    //si cambiamos el NULL por una variable nos devolbera la direccion de la varible donde esta ubicado el thread

    return 0;
}
