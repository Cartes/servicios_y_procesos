#ifndef __HILO_H__
#define __HILO_H__

#include <stdlib.h>

struct TPila {
    char character;
    int count=0;
};

#ifdef __cplusplus
extern "C" {
#endif


    void* char_print (void* parameters);


#ifdef __cplusplus
}
#endif

#endif
