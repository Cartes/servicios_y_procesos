#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <strings.h>

void menu () {
    system("toilet -fpagga --metal MENU\n");
    printf("1.Crear hilos\n");
    printf("2.Eliminar ultimo hilo\n");
    printf("3.Cerrar la tienda\n");
}

struct Tpila {
    int count= 0;
    long int id_th[100];
};

void* aumentar (void* creados)
{
    struct Tpila* p = (struct Tpila*) creados;

    for (int i=0; i<p->count; i++)
    {
        fputc(p->id_th[i],stderr);

      return NULL;
    }
}

void push(struct Tpila *p, long id) //*p direccion de una pila si ponemos &p ya pasamos por referencia y nos quitamos las flechas
{
    p->id_th[p->count] = id;
    p->count++;
}

long create_thread(pthread_t thread_ids) {
    struct Tpila thread_ids_args;

    pthread_create(&thread_ids, NULL, &aumentar, &thread_ids_args);
    printf("Ha creado el hilo con su numero: %li\n", thread_ids);
    return thread_ids;
}
void CerrarHilo(pthread_t thread_ids) {

    pthread_join (thread_ids,NULL);
    printf("He cerrado el hilo: %li\n", thread_ids);
}
int main () {


    struct Tpila pila;
    bzero  (&pila, sizeof(pila));


    int opcion;
    pthread_t thread_ids;

    do{

    menu();
    printf("\n\nQue opcion eliges:");
    scanf(" %d", &opcion);

    switch(opcion){
        case 1:
            create_thread(thread_ids);
            push(&pila, thread_ids);
            break;

        case 2:
            printf("La cima es : %i\n",pila.count);
            break;

        case 3:
            for (int i=0; i<pila.count; i++){
            CerrarHilo(pila.id_th[i]);
    }
            break;
    }

    }while(opcion != 4);

    return 0;
}
