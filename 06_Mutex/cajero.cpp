#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#define MAX_BUYS 10

FILE *pf = NULL;

#define LOUNGE "archivo.txt"
#define MAX 0x100

struct TStack {
    int summit = 0;
    int data[MAX];
    int base;
};

void push (struct TStack *s, pthread_t new_id)
{
    s->data[s->summit++] = new_id;
}


void * buy (void* params)
{

    int n_buys = * (int *) params;

    for (int i=n_buys; i>0; i--)
    {
        fprintf(pf, "[ID: %lu] Quedan %i compras.\n.", pthread_self (), i);
        fflush(pf);
        usleep (10000);
    }
    free (params);
}

pthread_t new_clien() {
    pthread_t thread_id;

    int *n_buys = (int *) malloc(sizeof (int));
    *n_buys = rand () % MAX_BUYS + 1;

    pthread_create (&thread_id, NULL, &buy, n_buys);

    return thread_id;
}


void begin (struct TStack *st_client)
  {
      pf = fopen (LOUNGE, "w");
      if(!pf){
          fprintf(stderr, "No hemos encontrado el fich\n");
          exit(1);
      }
      //init(st_client);
      srand(time(NULL));
  }

int main()
{

    struct TStack c;

    for (int i =0; i<5; i++)
        push(&c, new_clien ());

    return EXIT_SUCCESS;
}
