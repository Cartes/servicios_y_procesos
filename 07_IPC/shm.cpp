#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/stat.h>




int main(){
    int segment_id;
    char* shared_memory;
    struct shmid_ds shmbuffer;
    int segment_size;
    const int shared_segment_size = 0x6400;

    /*alojamos la memoria */
    segment_id = shmget (IPC_PRIVATE, shared_segment_size, IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);

    /*juntamos la memoria compartida*/
    shared_memory = (char*) shmat (segment_id, 0, 0);
    printf("la memoria compartida tiene la dirección %p\n", shared_memory);

    /*Determina el tamaño del segmento*/
    shmctl (segment_id, IPC_STAT, &shmbuffer);
    segment_size = shmbuffer.shm_segsz;
    printf("tamaño del segmento: %d\n", segment_size);

    /*Escribe en el string  */
    sprintf(shared_memory, "Hello");
    shmdt(shared_memory);

    shared_memory = (char*) shmat (segment_id, (void*) 0x5000000, 0);
    printf("%p", shared_memory);
    printf("%s\n", shared_memory);
    shmdt (shared_memory);

    shmctl (segment_id, IPC_RMID, 0);

    return EXIT_SUCCESS;

}
