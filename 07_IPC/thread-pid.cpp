#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void* thread_function (void* arg)
{
    fprintf(stderr, "hijo thread pid is %d\n", (int) getpid ());
    while(1);
    return NULL;
}

int main() {
    pthread_t thread;
    fprintf(stderr, "main hilo pid is %d\n", (int) getpid ());
    pthread_create (&thread, NULL, &thread_function, NULL);

    while(1);
    return 0;
}
