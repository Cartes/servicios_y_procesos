#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;
int x;


void initialize_flag(){ // inicialización
    pthread_mutex_init (&thread_flag_mutex, NULL);
    pthread_cond_init (&thread_flag_cv, NULL);
    thread_flag = 0;
    x=0;
}

void set_thread_flag (int flag_value){  //Cambiamos valor de la vandera
    pthread_mutex_lock (&thread_flag_mutex);
    thread_flag = flag_value;
    pthread_cond_signal(&thread_flag_cv);
    pthread_mutex_unlock (&thread_flag_mutex);
}

void do_work(){
    printf("tiempo:%i\n", x-1);
    thread_flag = 0; //Bajamos la bandera
}

void* tip(void* args){ //Funcion tiempo
    while(1){
    sleep(1);
    if (x % 5 == 0 )
        set_thread_flag (x);
    x++;
    }
    return NULL;
}

void* thread_function (void* thread_arg){ //Funcion comprueba que la bandera este levantada o bajada
    while(1){
        pthread_mutex_lock(&thread_flag_mutex);
        while(!thread_flag)
            pthread_cond_wait(&thread_flag_cv, &thread_flag_mutex);
        pthread_mutex_unlock(&thread_flag_mutex);
        do_work();
    }
    return NULL;
}

int main(){

    pthread_t ID;
    pthread_t ID2;

    initialize_flag();

    pthread_create(&ID, NULL, &tip, NULL); //Hilo tiempo
    pthread_create(&ID2, NULL, &thread_function, NULL); //Comprobacion bandera

    pthread_join(ID, NULL);
    pthread_join(ID2, NULL);

    return 0;
}
