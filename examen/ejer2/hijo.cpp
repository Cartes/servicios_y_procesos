#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define DELAY 2

sig_atomic_t sigusr1_count = 0;

void handler (int signal_number){
    ++sigusr1_count;
}

int main() {
    int a;
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = &handler;
    sigaction (SIGUSR1, &sa, NULL);

    while (sigusr1_count < 3){
        sleep(DELAY);   //duerme programa
        printf("Pasa %i\n", sigusr1_count);
    }

    printf("SIGUSR1 repetido %i veces \n", sigusr1_count);
}
