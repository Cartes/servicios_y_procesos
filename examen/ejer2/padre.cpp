#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define DELAY 1
#define HIJO_PROGRAMA "./hijo"
#define N_SIGNAL 3

int spawn(char* program, char** arg_list){
    pid_t child_pid;
    child_pid = fork ();

    if (child_pid != 0){
        return child_pid;
    }
    else
    {
        execvp(program, arg_list);
        fprintf(stderr, "Error");
        abort();
    }
}

int main() {
    pid_t pid_child;

    int child_status;
    char* arg_list[] = {(char *) HIJO_PROGRAMA, NULL};

    pid_child = spawn ((char *) HIJO_PROGRAMA, arg_list);

    int i=0;

    while (i < N_SIGNAL){
        sleep(DELAY);
        i++;
        kill(pid_child, SIGUSR1);
        printf("Enviar: %i señales\n", i);
    }

    wait(&child_status);

    if (WIFEXITED (child_status) )
        printf("Ha salido bien el hijo");
    else
        printf("No ha salido bien");

    return 0;
}
