#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


sig_atomic_t sigusr_count = 0;
int child_status;
pid_t child_pid;

void handler (int signal_number)
{
    ++sigusr_count;
}

void spawn()
{
  child_pid = fork ();
  if (child_pid != 0)
  {
      /*Padre*/
      handler(1);
      handler(1);
      handler(1);
  }
  else
  {
      /*El hijo */
      if (sigusr_count <= 3)
      {
      printf("entra hijo\n");
      }
  }
}

//Redacta un programa que cree un proceso hijo que muera al recibir tres veces la señal sigusr1
//que le enviará el padre. Asegurate de recibir asincronamente la muerte del hijo
//para evitar que quede zombie

int main()
{
    int child_status;
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = &handler;
    sigaction (SIGUSR1, &sa, NULL);


        spawn();
        wait(&child_status);

    printf("He enviado %i señales\n", sigusr_count);


    return EXIT_SUCCESS;
}
